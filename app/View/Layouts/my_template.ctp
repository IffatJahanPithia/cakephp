<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
		<title>e-exam</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?php echo $this->Html->css('style'); ?>
        <?php $this->fetch('css'); ?>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta charset="UTF-8">    
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="header">
            
            <div class="col-md-3 box">
                <h3>Test your skill</h3>
            </div>
            
            <div class="col-md-7 box">
            </div>
            
            <div class="col-md-2 box">
            <!-- Trigger the modal with a button -->
                <button class="btn success butten" id="myBtn"><p>Sign-in</p></button>
                <!--Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                  <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" style="padding:35px 50px;">
                                <button type="submit" name="login" class="close" data-dismiss="modal">&times;</button>
                                <h4><span class="glyphicon glyphicon-lock"></span> Login</h4>
                            </div>
                            <div class="modal-body" style="padding:40px 50px;">
                               <!-- Modal-form content--> 
                                <form role="form" id="mdclass" class="mdclass" action="register.php" method="post" style="background-color: whitesmoke;">
                                    <div class="form-group">
                                        <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
                                        <input type="text" class="form-control" id="usrname" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                                        <input type="text" class="form-control" id="psw" placeholder="Enter password">
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="" checked>Remember me</label>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block" style="background-color: #000000;"><span class="glyphicon glyphicon-off"></span> Login</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                                <p>Not a member? <a href="#">Sign Up</a></p>
                                <p>Forgot <a href="#">Password?</a></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function(){
                $("#myBtn").click(function(){
                    $("#myModal").modal();
                });
            });
        </script>
        
        
        
        
        
        
        <div class="body" id="feedback">
            
            <?php echo $this->fetch('content'); ?>
           
                             
        </div>
        
        
        
        
         
        <footer>
            <div class="footer">
                <div class="col-md-3 foot">
                    <a href=""><p>About us</p></a>
                </div>
                <div class="col-md-3 foot">
                    <a href="#" id="admin"><p>Admin login</p></a>
                    <div class="container">

          <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
             <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header" style="padding:35px 50px;">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4><span class="glyphicon glyphicon-lock"></span> Login</h4>
                                    </div>
                                    <div class="modal-body" style="padding:40px 50px;">
                                        <form role="form">
                                            <div class="form-group">
                                                <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
                                                <input type="text" class="form-control" id="usrname" placeholder="Enter email">
                                            </div>
                                            <div class="form-group">
                                                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                                                <input type="text" class="form-control" id="psw" placeholder="Enter password">
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="" checked>Remember me</label>
                                            </div>
                                            <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Login</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                                        <p>Not a member? <a href="#">Sign Up</a></p>
                                        <p>Forgot <a href="#">Password?</a></p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <script>
                        $(document).ready(function(){
                            $("#admin").click(function(){
                                $("#myModal").modal();
                            });
                        });
                    </script>
                </div>
                <div class="col-md-3 foot">
                    <a href=""><p>Developers</p></a>
                </div>
                <div class="col-md-3 foot">
                    <a href="feedback.php"><p>Feedback</p></a>
                </div>
            </div>
         </footer>
    </body>
</html>



        
        
        
        
        
        
        
        
        
        