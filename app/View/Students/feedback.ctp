
    <div class="feedback" id="content">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6 feedback-div">
            <form role="form" class="feedback-form">
                <div class="h">
                    <h2>FEEDBACK or REPORT A PROBLEM</h2>
                    <h5>You can submit your feedback by feeling the entries below.</h5>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter your name">
                </div>
               <div class="form-group">
                   <label for="sub">Subject</label>
                   <input type="text" class="form-control" id="sub" placeholder="Enter subject">
               </div>
               <div class="form-group">
                   <label for="mail">E-mail</label>
                   <input type="text" class="form-control" id="mail" placeholder="Enter your E-mail id">
               </div>
               <div class="form-group">
                   <label for="txtarea">Message</label><br>
                   <textarea class="form-control" rows="5" cols="80" placeholder="Give your feedback here..."></textarea>
               </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block" style="font-family: comic sans MS;"> Submit</button>
                
                </div>
            </form> 
            
        </div>
        <div class="col-md-3">
            
        </div>
    </div>